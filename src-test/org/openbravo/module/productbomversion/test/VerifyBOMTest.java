/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2018-2021 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.module.productbomversion.test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.weld.test.WeldBaseTest;
import org.openbravo.client.kernel.RequestContext;
import org.openbravo.dal.core.DalUtil;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductBOM;
import org.openbravo.module.productbomversion.test.api.BOMUtils;

/**
 * Tests VerifyBillOfMaterials action handler .<br>
 * 
 */
public class VerifyBOMTest extends WeldBaseTest {

  public static final String BOMA_PRODUCT_ID = "4028E6C72959682B01295ADC1F580233";

  public static final String FINAL_GOOD_A_ID = "4028E6C72959682B01295ADC1D07022A";
  public static final String FINAL_GOOD_B_ID = "4028E6C72959682B01295ADC1DC2022E";

  @Before
  public void setUp() throws Exception {
    super.setUp();
    BOMVersionOBContext.setTestUserContext();
    VariablesSecureApp vars = null;
    vars = new VariablesSecureApp(OBContext.getOBContext().getUser().getId(), OBContext
        .getOBContext().getCurrentClient().getId(), OBContext.getOBContext()
        .getCurrentOrganization().getId(), OBContext.getOBContext().getRole().getId(), OBContext
        .getOBContext().getLanguage().getLanguage());
    RequestContext.get().setVariableSecureApp(vars);
  }

  /**
   * A test to verify a product with empty BOM
   */
  @Test
  public void testOBPBOMV010() throws JSONException, NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException {

    Product productWithEmptyBom = null;
    try {
      Product bomAsample = OBDal.getInstance().get(Product.class, BOMA_PRODUCT_ID);
      productWithEmptyBom = (Product) DalUtil.copy(bomAsample, false);
      // Change attributes
      productWithEmptyBom.setValue(Product.PROPERTY_SEARCHKEY, "BOM-A1");
      productWithEmptyBom.setValue(Product.PROPERTY_NAME, "BOM-A1");
      productWithEmptyBom.setUPCEAN(null);
      OBDal.getInstance().save(productWithEmptyBom);
      OBDal.getInstance().flush();
      assertThat(productWithEmptyBom.isBOMVerified(), equalTo(Boolean.FALSE));

      JSONObject message = BOMUtils.verifyBOM(productWithEmptyBom.getId())
          .getJSONArray("responseActions").getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "error",
          "Verify that you have included one or more products in tab Bill of Materials");

      // reload the properties
      productWithEmptyBom = OBDal.getInstance().get(Product.class, productWithEmptyBom.getId());

      assertThat(productWithEmptyBom.isBOMVerified(), equalTo(Boolean.FALSE));
    } finally {
      if (productWithEmptyBom != null) {
        BOMUtils.recordsCleanUp(Product.class,
            Product.PROPERTY_ID + "='" + productWithEmptyBom.getId() + "'");
      }
    }
  }

  /**
   * A test to verify simple product with BOM
   */
  @Test
  public void testOBPBOMV020() throws JSONException, NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException {

    Product productWithBom = null;
    try {
      Product bomAsample = OBDal.getInstance().get(Product.class, BOMA_PRODUCT_ID);
      productWithBom = (Product) DalUtil.copy(bomAsample, false);
      // Change attributes
      productWithBom.setValue(Product.PROPERTY_SEARCHKEY, "BOM-A1");
      productWithBom.setValue(Product.PROPERTY_NAME, "BOM-A1");
      productWithBom.setUPCEAN(null);
      assertThat(productWithBom.isBOMVerified(), equalTo(Boolean.FALSE));
      OBDal.getInstance().save(productWithBom);
      OBDal.getInstance().flush();

      // Create material
      BOMUtils.createMaterial(10L, productWithBom,
          OBDal.getInstance().get(Product.class, FINAL_GOOD_A_ID), BigDecimal.ONE, BigDecimal.ONE);

      JSONObject message = BOMUtils.verifyBOM(productWithBom.getId())
          .getJSONArray("responseActions").getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "success", "Process completed successfully");
      // reload the properties
      productWithBom = OBDal.getInstance().get(Product.class, productWithBom.getId());
      assertThat(productWithBom.isBOMVerified(), equalTo(Boolean.TRUE));
    } finally {

      if (productWithBom != null) {
        BOMUtils.recordsCleanUp(ProductBOM.class, ProductBOM.PROPERTY_PRODUCT + "='"
            + productWithBom.getId() + "'");
        BOMUtils.recordsCleanUp(Product.class, Product.PROPERTY_ID + "='" + productWithBom.getId()
            + "'");
      }
    }
  }

  /**
   * A test to verify materials with negative BOM quantity
   */
  @Test
  public void testOBPBOMV030() throws JSONException, NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException {

    Product productWithBom = null;
    try {
      Product bomAsample = OBDal.getInstance().get(Product.class, BOMA_PRODUCT_ID);
      productWithBom = (Product) DalUtil.copy(bomAsample, false);
      // Change attributes
      productWithBom.setValue(Product.PROPERTY_SEARCHKEY, "BOM-A1");
      productWithBom.setValue(Product.PROPERTY_NAME, "BOM-A1");
      productWithBom.setUPCEAN(null);
      assertThat(productWithBom.isBOMVerified(), equalTo(Boolean.FALSE));

      OBDal.getInstance().save(productWithBom);
      OBDal.getInstance().flush();

      // Create material
      BOMUtils.createMaterial(10L, productWithBom,
          OBDal.getInstance().get(Product.class, FINAL_GOOD_A_ID), BigDecimal.ONE.negate(),
          BigDecimal.ONE);

      JSONObject message = BOMUtils.verifyBOM(productWithBom.getId())
          .getJSONArray("responseActions").getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "error",
          "It is not allowed to insert negative quantities in BOM Products");
      // reload the properties
      productWithBom = OBDal.getInstance().get(Product.class, productWithBom.getId());
      assertThat(productWithBom.isBOMVerified(), equalTo(Boolean.FALSE));
    } finally {
      if (productWithBom != null) {
        BOMUtils.recordsCleanUp(ProductBOM.class, ProductBOM.PROPERTY_PRODUCT + "='"
            + productWithBom.getId() + "'");
        BOMUtils.recordsCleanUp(Product.class, Product.PROPERTY_ID + "='" + productWithBom.getId()
            + "'");
      }
    }
  }

  /**
   * A test to check that isBOMVerify property remain to TRUE when all material are removed
   */
  @Test
  public void testOBPBOMV060() throws JSONException, NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException {

    Product productWithBom = null;
    try {
      Product bomAsample = OBDal.getInstance().get(Product.class, BOMA_PRODUCT_ID);
      productWithBom = (Product) DalUtil.copy(bomAsample, false);
      // Change attributes
      productWithBom.setValue(Product.PROPERTY_SEARCHKEY, "BOM-A1");
      productWithBom.setValue(Product.PROPERTY_NAME, "BOM-A1");
      productWithBom.setUPCEAN(null);
      assertThat(productWithBom.isBOMVerified(), equalTo(Boolean.FALSE));

      OBDal.getInstance().save(productWithBom);
      OBDal.getInstance().flush();

      // Create material
      BOMUtils.createMaterial(10L, productWithBom,
          OBDal.getInstance().get(Product.class, FINAL_GOOD_A_ID), BigDecimal.ONE, BigDecimal.ONE);

      JSONObject message = BOMUtils.verifyBOM(productWithBom.getId())
          .getJSONArray("responseActions").getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "success", "Process completed successfully");

      // reload the properties
      productWithBom = OBDal.getInstance().get(Product.class, productWithBom.getId());
      assertThat(productWithBom.isBOMVerified(), equalTo(Boolean.TRUE));
    } finally {
      if (productWithBom != null) {
        BOMUtils.recordsCleanUp(ProductBOM.class, ProductBOM.PROPERTY_PRODUCT + "='"
            + productWithBom.getId() + "'");
        // reload the properties
        // productWithBom = OBDal.getInstance().get(Product.class, productWithBom.getId());
        // TODO: if all material are removed then isBOMVerified property must be false.
        assertThat(productWithBom.isBOMVerified(), equalTo(Boolean.TRUE));
        BOMUtils.recordsCleanUp(Product.class, Product.PROPERTY_ID + "='" + productWithBom.getId()
            + "'");
      }
    }
  }

  /**
   * A test to check that an error show up when it’s created a Product with BOM that contain it self
   * as material
   */
  @Test
  public void testOBPBOMV080() throws JSONException, NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException {

    Product productWithBom = null;
    try {
      Product bomAsample = OBDal.getInstance().get(Product.class, BOMA_PRODUCT_ID);
      productWithBom = (Product) DalUtil.copy(bomAsample, false);
      // Change attributes
      productWithBom.setValue(Product.PROPERTY_SEARCHKEY, "BOM-A1");
      productWithBom.setValue(Product.PROPERTY_NAME, "BOM-A1");
      productWithBom.setUPCEAN(null);
      assertThat(productWithBom.isBOMVerified(), equalTo(Boolean.FALSE));
      OBDal.getInstance().save(productWithBom);

      // Create material
      BOMUtils.createMaterial(10L, productWithBom,
          OBDal.getInstance().get(Product.class, FINAL_GOOD_A_ID), BigDecimal.ONE, BigDecimal.ONE);

      // Check 1st Verify BOM
      JSONObject message = BOMUtils.verifyBOM(productWithBom.getId())
          .getJSONArray("responseActions").getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "success", "Process completed successfully");

      // reload the properties
      productWithBom = OBDal.getInstance().get(Product.class, productWithBom.getId());
      assertThat(productWithBom.isBOMVerified(), equalTo(Boolean.TRUE));

      // Create it self as material
      BOMUtils.createMaterial(20L, productWithBom, productWithBom, BigDecimal.ONE, BigDecimal.ONE);

      message = BOMUtils.verifyBOM(productWithBom.getId()).getJSONArray("responseActions")
          .getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "error",
          "BOM Tree Contains a Cycle Please Verify BOM");

      // reload the properties
      productWithBom = OBDal.getInstance().get(Product.class, productWithBom.getId());
      assertThat(productWithBom.isBOMVerified(), equalTo(Boolean.FALSE));
    } finally {
      if (productWithBom != null) {
        BOMUtils.recordsCleanUp(ProductBOM.class, ProductBOM.PROPERTY_PRODUCT + "='"
            + productWithBom.getId() + "'");
        BOMUtils.recordsCleanUp(Product.class, Product.PROPERTY_ID + "='" + productWithBom.getId()
            + "'");
      }
    }
  }

  /**
   * A test to check that an error show up when it’s created a product with BOM that contain a
   * material with a substitute with cycle
   */
  @Test
  public void testOBPBOMV090() throws JSONException, NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException {

    Product productWithBom = null;
    Product simpleMaterialA2 = null;
    try {
      Product bomAsample = OBDal.getInstance().get(Product.class, BOMA_PRODUCT_ID);
      productWithBom = (Product) DalUtil.copy(bomAsample, false);

      // Change attributes
      productWithBom.setValue(Product.PROPERTY_SEARCHKEY, "BOM-A2");
      productWithBom.setValue(Product.PROPERTY_NAME, "BOM-A2");
      productWithBom.setUPCEAN(null);
      productWithBom.setObpbomvIssubstitute(true);
      OBDal.getInstance().save(productWithBom);

      assertThat(productWithBom.isBOMVerified(), equalTo(Boolean.FALSE));

      // Create material
      BOMUtils.createMaterial(10L, productWithBom,
          OBDal.getInstance().get(Product.class, FINAL_GOOD_A_ID), BigDecimal.ONE, BigDecimal.ONE);

      // Create simple material product
      Product simpleMaterial = OBDal.getInstance().get(Product.class, FINAL_GOOD_B_ID);
      simpleMaterialA2 = (Product) DalUtil.copy(simpleMaterial, false);

      // Change attributes
      simpleMaterialA2.setValue(Product.PROPERTY_SEARCHKEY, "SimpleMaterial-A2");
      simpleMaterialA2.setValue(Product.PROPERTY_NAME, "SimpleMaterial-A2");
      simpleMaterialA2.setUPCEAN(null);
      OBDal.getInstance().save(simpleMaterialA2);
      OBDal.getInstance().flush();

      // Check add substitute process
      JSONObject message = BOMUtils
          .addSubstitute(simpleMaterialA2.getId(), "S2", productWithBom.getId())
          .getJSONArray("responseActions").getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "success", "Process completed successfully");

      // Add simpleMaterialA2 as material of productWithBom
      BOMUtils
          .createMaterial(10L, productWithBom, simpleMaterialA2, BigDecimal.ONE, BigDecimal.ONE);

      OBDal.getInstance().commitAndClose();

      message = BOMUtils.verifyBOM(productWithBom.getId()).getJSONArray("responseActions")
          .getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "error",
          "BOM Tree Contains a Cycle Please Verify BOM");

      // reload the properties
      productWithBom = OBDal.getInstance().get(Product.class, productWithBom.getId());
      assertThat(productWithBom.isBOMVerified(), equalTo(Boolean.FALSE));
    } finally {
      if (productWithBom != null && simpleMaterialA2 != null) {
        BOMUtils.recordsCleanUp(ProductBOM.class, ProductBOM.PROPERTY_PRODUCT + "='"
            + productWithBom.getId() + "'");

        // reload the properties
        productWithBom = OBDal.getInstance().get(Product.class, productWithBom.getId());

        BOMUtils.recordsCleanUp(Product.class,
            Product.PROPERTY_ID + "='" + simpleMaterialA2.getId() + "'");
        BOMUtils.recordsCleanUp(Product.class, Product.PROPERTY_ID + "='" + productWithBom.getId()
            + "'");
      }
    }
  }

  /**
   * A test to check that an error show up when it’s created a product with BOM that contain a 2nd
   * level cycle
   */
  @Test
  public void testOBPBOMV100() throws JSONException, NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException {

    Product productWithBomB3 = null;
    Product productWithBomA3 = null;
    try {
      Product bomAsample = OBDal.getInstance().get(Product.class, BOMA_PRODUCT_ID);
      productWithBomA3 = (Product) DalUtil.copy(bomAsample, false);

      // Change attributes
      productWithBomA3.setValue(Product.PROPERTY_SEARCHKEY, "BOM-A3");
      productWithBomA3.setValue(Product.PROPERTY_NAME, "BOM-A3");
      productWithBomA3.setUPCEAN(null);
      OBDal.getInstance().save(productWithBomA3);

      assertThat(productWithBomA3.isBOMVerified(), equalTo(Boolean.FALSE));

      // Create material
      BOMUtils.createMaterial(10L, productWithBomA3,
          OBDal.getInstance().get(Product.class, FINAL_GOOD_A_ID), BigDecimal.ONE, BigDecimal.ONE);

      // Check 1st Verify BOM
      JSONObject message = BOMUtils.verifyBOM(productWithBomA3.getId())
          .getJSONArray("responseActions").getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "success", "Process completed successfully");

      // reload the properties
      productWithBomA3 = OBDal.getInstance().get(Product.class, productWithBomA3.getId());
      assertThat(productWithBomA3.isBOMVerified(), equalTo(Boolean.TRUE));

      // Create another product with BOM
      productWithBomB3 = (Product) DalUtil.copy(bomAsample, false);

      // Change attributes
      productWithBomB3.setValue(Product.PROPERTY_SEARCHKEY, "BOM-B3");
      productWithBomB3.setValue(Product.PROPERTY_NAME, "BOM-B3");
      productWithBomB3.setUPCEAN(null);
      OBDal.getInstance().save(productWithBomB3);

      assertThat(productWithBomB3.isBOMVerified(), equalTo(Boolean.FALSE));

      // Create material
      BOMUtils.createMaterial(10L, productWithBomB3, productWithBomA3, BigDecimal.ONE,
          BigDecimal.ONE);

      // Check 2nd Verify BOM
      message = BOMUtils.verifyBOM(productWithBomB3.getId()).getJSONArray("responseActions")
          .getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "success", "Process completed successfully");

      // reload the properties
      productWithBomB3 = OBDal.getInstance().get(Product.class, productWithBomB3.getId());
      assertThat(productWithBomB3.isBOMVerified(), equalTo(Boolean.TRUE));

      // Create cycle material
      productWithBomA3 = OBDal.getInstance().get(Product.class, productWithBomA3.getId());
      BOMUtils.createMaterial(10L, productWithBomA3, productWithBomB3, BigDecimal.ONE,
          BigDecimal.ONE);

      message = BOMUtils.verifyBOM(productWithBomA3.getId()).getJSONArray("responseActions")
          .getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "error",
          "BOM Tree Contains a Cycle Please Verify BOM");
      // reload the properties
      productWithBomA3 = OBDal.getInstance().get(Product.class, productWithBomA3.getId());
      assertThat(productWithBomA3.isBOMVerified(), equalTo(Boolean.FALSE));

      // TODO: This must be checked because productWithBomB3->productWithBomA3->productWithBomB3
      // cycle
      // is also formed. Commenting because at this point Verify BOM button is not visible.
      // message = BOMUtils.verifyBOM(productWithBomB3.getId()).getJSONArray("responseActions")
      // .getJSONObject(0).getJSONObject("showMsgInProcessView");
      // BOMVersionTestSuite.assertResponse(message, "error",
      // "BOM Tree Contains a Cycle Please Verify BOM");
      // reload the properties
      // productWithBomB3 = OBDal.getInstance().get(Product.class, productWithBomB3.getId());
      // assertThat(productWithBomB3.isBOMVerified(), equalTo(Boolean.FALSE));

    } finally {
      if (productWithBomA3 != null && productWithBomB3 != null) {
        BOMUtils.recordsCleanUp(ProductBOM.class, ProductBOM.PROPERTY_PRODUCT + "='"
            + productWithBomA3.getId() + "'");
        BOMUtils.recordsCleanUp(ProductBOM.class, ProductBOM.PROPERTY_PRODUCT + "='"
            + productWithBomB3.getId() + "'");
        productWithBomA3 = OBDal.getInstance().get(Product.class, productWithBomA3.getId());
        BOMUtils.recordsCleanUp(Product.class,
            Product.PROPERTY_ID + "='" + productWithBomA3.getId() + "'");
        productWithBomB3 = OBDal.getInstance().get(Product.class, productWithBomB3.getId());
        BOMUtils.recordsCleanUp(Product.class,
            Product.PROPERTY_ID + "='" + productWithBomB3.getId() + "'");
      }
    }
  }

  /**
   * A test to check that an error show up when it’s created a product with BOM that contain a 2nd
   * level cycle through a substitute
   */
  @Test
  public void testOBPBOMV110() throws JSONException, NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException {

    Product productWithBomA4 = null;
    Product productWithBomB4 = null;
    Product productWithBomC4 = null;

    try {
      Product bomAsample = OBDal.getInstance().get(Product.class, BOMA_PRODUCT_ID);

      // Create BOM-A4
      productWithBomA4 = (Product) DalUtil.copy(bomAsample, false);

      // Change attributes
      productWithBomA4.setValue(Product.PROPERTY_SEARCHKEY, "BOM-A4");
      productWithBomA4.setValue(Product.PROPERTY_NAME, "BOM-A4");
      productWithBomA4.setUPCEAN(null);
      productWithBomA4.setObpbomvIssubstitute(true);
      OBDal.getInstance().save(productWithBomA4);

      assertThat(productWithBomA4.isBOMVerified(), equalTo(Boolean.FALSE));

      // Add non BO material
      BOMUtils.createMaterial(10L, productWithBomA4,
          OBDal.getInstance().get(Product.class, FINAL_GOOD_A_ID), BigDecimal.ONE, BigDecimal.ONE);

      // Check 1st Verify BOM
      JSONObject message = BOMUtils.verifyBOM(productWithBomA4.getId())
          .getJSONArray("responseActions").getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "success", "Process completed successfully");

      // reload the properties
      productWithBomA4 = OBDal.getInstance().get(Product.class, productWithBomA4.getId());
      assertThat(productWithBomA4.isBOMVerified(), equalTo(Boolean.TRUE));

      // Create BOM-B4
      productWithBomB4 = (Product) DalUtil.copy(bomAsample, false);

      // Change attributes
      productWithBomB4.setValue(Product.PROPERTY_SEARCHKEY, "BOM-B4");
      productWithBomB4.setValue(Product.PROPERTY_NAME, "BOM-B4");
      productWithBomB4.setUPCEAN(null);
      OBDal.getInstance().save(productWithBomB4);

      assertThat(productWithBomB4.isBOMVerified(), equalTo(Boolean.FALSE));

      // Add non BO material
      BOMUtils.createMaterial(10L, productWithBomB4,
          OBDal.getInstance().get(Product.class, FINAL_GOOD_B_ID), BigDecimal.ONE, BigDecimal.ONE);

      // Add substitute BOM-A4
      message = BOMUtils.addSubstitute(productWithBomB4.getId(), "S4", productWithBomA4.getId())
          .getJSONArray("responseActions").getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "success", "Process completed successfully");

      // Create BOM-C4
      productWithBomC4 = (Product) DalUtil.copy(bomAsample, false);

      // Change attributes
      productWithBomC4.setValue(Product.PROPERTY_SEARCHKEY, "BOM-C4");
      productWithBomC4.setValue(Product.PROPERTY_NAME, "BOM-C4");
      productWithBomC4.setUPCEAN(null);
      OBDal.getInstance().save(productWithBomC4);

      assertThat(productWithBomC4.isBOMVerified(), equalTo(Boolean.FALSE));

      // Add BOM-B4 as material of BOM-C4
      BOMUtils.createMaterial(10L, productWithBomC4, productWithBomB4, BigDecimal.ONE,
          BigDecimal.ONE);

      // Add BOM-C4 as material of BOM-A4
      productWithBomA4 = OBDal.getInstance().get(Product.class, productWithBomA4.getId());
      BOMUtils.createMaterial(20L, productWithBomA4, productWithBomC4, BigDecimal.ONE,
          BigDecimal.ONE);

      // Check BOM-C4 for cycle
      message = BOMUtils.verifyBOM(productWithBomC4.getId()).getJSONArray("responseActions")
          .getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "error",
          "BOM Tree Contains a Cycle Please Verify BOM");

      // reload the properties
      productWithBomC4 = OBDal.getInstance().get(Product.class, productWithBomC4.getId());
      assertThat(productWithBomC4.isBOMVerified(), equalTo(Boolean.FALSE));

      // Check BOM-B4 for no cycle
      message = BOMUtils.verifyBOM(productWithBomB4.getId()).getJSONArray("responseActions")
          .getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "success", "Process completed successfully");

      // reload the properties
      productWithBomB4 = OBDal.getInstance().get(Product.class, productWithBomB4.getId());
      assertThat(productWithBomB4.isBOMVerified(), equalTo(Boolean.TRUE));

      // Check BOM-A4 for cycle
      message = BOMUtils.verifyBOM(productWithBomA4.getId()).getJSONArray("responseActions")
          .getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "error",
          "BOM Tree Contains a Cycle Please Verify BOM");

      // reload the properties
      productWithBomA4 = OBDal.getInstance().get(Product.class, productWithBomA4.getId());
      assertThat(productWithBomA4.isBOMVerified(), equalTo(Boolean.FALSE));

    } finally {
      if (productWithBomA4 != null && productWithBomB4 != null && productWithBomC4 != null) {
        BOMUtils.recordsCleanUp(ProductBOM.class, ProductBOM.PROPERTY_PRODUCT + "='"
            + productWithBomA4.getId() + "'");
        BOMUtils.recordsCleanUp(ProductBOM.class, ProductBOM.PROPERTY_PRODUCT + "='"
            + productWithBomC4.getId() + "'");
        productWithBomC4 = OBDal.getInstance().get(Product.class, productWithBomC4.getId());
        BOMUtils.recordsCleanUp(Product.class,
            Product.PROPERTY_ID + "='" + productWithBomC4.getId() + "'");
        BOMUtils.recordsCleanUp(Product.class,
            Product.PROPERTY_ID + "='" + productWithBomB4.getId() + "'");
        productWithBomA4 = OBDal.getInstance().get(Product.class, productWithBomA4.getId());
        BOMUtils.recordsCleanUp(Product.class,
            Product.PROPERTY_ID + "='" + productWithBomA4.getId() + "'");
      }
    }
  }

  /**
   * A test to check that an error show up when it’s created a product with BOM that contain a
   * material with a substitute that implies a cycle
   */
  @Test
  public void testOBPBOMV120() throws JSONException, NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException {

    Product productWithBomA5 = null;
    Product productWithBomB5 = null;
    Product simpleMaterialB5 = null;

    try {
      Product bomAsample = OBDal.getInstance().get(Product.class, BOMA_PRODUCT_ID);

      // Create BOM-A5
      productWithBomA5 = (Product) DalUtil.copy(bomAsample, false);

      // Change attributes
      productWithBomA5.setValue(Product.PROPERTY_SEARCHKEY, "BOM-A5");
      productWithBomA5.setValue(Product.PROPERTY_NAME, "BOM-A5");
      productWithBomA5.setUPCEAN(null);
      productWithBomA5.setObpbomvIssubstitute(true);
      OBDal.getInstance().save(productWithBomA5);

      assertThat(productWithBomA5.isBOMVerified(), equalTo(Boolean.FALSE));

      // Add non BOM material
      BOMUtils.createMaterial(10L, productWithBomA5,
          OBDal.getInstance().get(Product.class, FINAL_GOOD_A_ID), BigDecimal.ONE, BigDecimal.ONE);

      // Create simple material product
      Product simpleMaterial = OBDal.getInstance().get(Product.class, FINAL_GOOD_B_ID);
      simpleMaterialB5 = (Product) DalUtil.copy(simpleMaterial, false);

      // Change attributes
      simpleMaterialB5.setValue(Product.PROPERTY_SEARCHKEY, "SimpleMaterial-A2");
      simpleMaterialB5.setValue(Product.PROPERTY_NAME, "SimpleMaterial-A2");
      simpleMaterialB5.setUPCEAN(null);
      OBDal.getInstance().save(simpleMaterialB5);
      OBDal.getInstance().flush();

      // Check add substitute process
      JSONObject message = BOMUtils
          .addSubstitute(simpleMaterialB5.getId(), "S5", productWithBomA5.getId())
          .getJSONArray("responseActions").getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "success", "Process completed successfully");

      // Create BOM-B5
      productWithBomB5 = (Product) DalUtil.copy(bomAsample, false);

      // Change attributes
      productWithBomB5.setValue(Product.PROPERTY_SEARCHKEY, "BOM-B5");
      productWithBomB5.setValue(Product.PROPERTY_NAME, "BOM-B5");
      productWithBomB5.setUPCEAN(null);
      productWithBomB5.setObpbomvIssubstitute(true);
      OBDal.getInstance().save(productWithBomB5);

      assertThat(productWithBomB5.isBOMVerified(), equalTo(Boolean.FALSE));

      // Add simpleMaterialB5 as material of productWithBomB5
      BOMUtils.createMaterial(10L, productWithBomB5, simpleMaterialB5, BigDecimal.ONE,
          BigDecimal.ONE);

      // Add BOM-B5 as material of BOM-A5
      BOMUtils.createMaterial(20L, productWithBomA5, productWithBomB5, BigDecimal.ONE,
          BigDecimal.ONE);

      // Check BOM-A5 for cycle
      message = BOMUtils.verifyBOM(productWithBomA5.getId()).getJSONArray("responseActions")
          .getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "error",
          "BOM Tree Contains a Cycle Please Verify BOM");

      // reload the properties
      productWithBomA5 = OBDal.getInstance().get(Product.class, productWithBomA5.getId());
      assertThat(productWithBomA5.isBOMVerified(), equalTo(Boolean.FALSE));

      // Check BOM-B5 for cycle
      message = BOMUtils.verifyBOM(productWithBomB5.getId()).getJSONArray("responseActions")
          .getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "error",
          "BOM Tree Contains a Cycle Please Verify BOM");
      // reload the properties
      productWithBomB5 = OBDal.getInstance().get(Product.class, productWithBomB5.getId());
      assertThat(productWithBomB5.isBOMVerified(), equalTo(Boolean.FALSE));

    } finally {
      if (productWithBomA5 != null && productWithBomB5 != null && simpleMaterialB5 != null) {
        BOMUtils.recordsCleanUp(ProductBOM.class, ProductBOM.PROPERTY_PRODUCT + "='"
            + productWithBomA5.getId() + "'");
        BOMUtils.recordsCleanUp(ProductBOM.class, ProductBOM.PROPERTY_PRODUCT + "='"
            + productWithBomB5.getId() + "'");
        productWithBomB5 = OBDal.getInstance().get(Product.class, productWithBomB5.getId());
        BOMUtils.recordsCleanUp(Product.class,
            Product.PROPERTY_ID + "='" + productWithBomB5.getId() + "'");
        simpleMaterialB5 = OBDal.getInstance().get(Product.class, simpleMaterialB5.getId());
        BOMUtils.recordsCleanUp(Product.class,
            Product.PROPERTY_ID + "='" + simpleMaterialB5.getId() + "'");
        productWithBomA5 = OBDal.getInstance().get(Product.class, productWithBomA5.getId());
        BOMUtils.recordsCleanUp(Product.class,
            Product.PROPERTY_ID + "='" + productWithBomA5.getId() + "'");
      }
    }
  }
}
