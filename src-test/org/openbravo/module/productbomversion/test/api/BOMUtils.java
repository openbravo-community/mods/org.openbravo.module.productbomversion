/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2018 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.module.productbomversion.test.api;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductBOM;
import org.openbravo.module.productbomversion.BOMVersion;
import org.openbravo.module.productbomversion.process.CreateBOMVersion;
import org.openbravo.module.productbomversion.process.SubstituteProcess;
import org.openbravo.module.productbomversion.process.VerifyBillOfMaterials;
import org.openbravo.module.productbomversion.process.VerifyBomVersionProcess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class for BOM version tests
 * 
 */
public class BOMUtils {

  final static private Logger log = LoggerFactory.getLogger(BOMUtils.class);

  /**
   * Utility method for clean record of OBDal table
   * 
   * @param obDalType
   *          class type of OBDal concrete table
   * @throws NoSuchMethodException
   *           , SecurityException, IllegalAccessException, IllegalArgumentException,
   *           InvocationTargetException
   */
  public static void recordsCleanUp(Class<? extends BaseOBObject> obDalType,
      final String whereClause) throws NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException {

    final OBQuery<?> obQuery = OBDal.getInstance().createQuery(obDalType, whereClause);
    List<?> records = obQuery.list();
    int i = 0;
    int size = records.size();
    OBDal.getInstance().commitAndClose();
    for (i = 0; i < size; ++i) {
      BaseOBObject item = (BaseOBObject) records.get(i);
      item = OBDal.getInstance().get(obDalType, item.getId());
      OBDal.getInstance().remove(item);
      OBDal.getInstance().commitAndClose();
    }
    OBDal.getInstance().commitAndClose();
    log.debug("Records deleted in " + obDalType.getName() + ":" + Integer.toString(i));
  }

  public static ProductBOM createMaterial(final Long lineNo, Product parentProduct,
      final Product bomProduct, BigDecimal bomQuantity, BigDecimal bomPrice) {

    ProductBOM material = OBProvider.getInstance().get(ProductBOM.class);
    material.setProduct(parentProduct);
    material.setLineNo(lineNo);
    material.setBOMProduct(bomProduct);
    material.setActive(true);
    material.setBOMQuantity(bomQuantity);
    material.setBomprice(bomPrice);
    List<ProductBOM> bomList = parentProduct.getProductBOMList();
    bomList.add(material);
    parentProduct.setProductBOMList(bomList);
    OBDal.getInstance().save(material);
    OBDal.getInstance().flush();
    OBDal.getInstance().commitAndClose();
    material = OBDal.getInstance().get(ProductBOM.class, material.getId());
    return material;
  }

  /**
   * General function to verify BOM of a product
   * 
   * @param productID
   *          Product ID string
   * @return JSON object returned by VerifyBillOfMaterials.doExecute method
   * @throws JSONException
   * @throws NoSuchMethodException
   * @throws SecurityException
   * @throws IllegalAccessException
   * @throws IllegalArgumentException
   * @throws InvocationTargetException
   * @see VerifyBillOfMaterials ActionHandler
   */
  public static JSONObject verifyBOM(final String productID) throws JSONException,
      NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
      InvocationTargetException {

    JSONObject content = new JSONObject();

    content.put("M_Product_ID", productID);

    JSONObject result = new VerifyBillOfMaterials() {
      public JSONObject publicExecute(Map<String, Object> _parameters, String _content) {
        return super.doExecute(_parameters, _content);
      }
    }.publicExecute(new HashMap<String, Object>(), content.toString());

    OBDal.getInstance().commitAndClose();
    return result;

  }

  /**
   * General function to add substitutes of a product
   * 
   * @param productID
   *          Product ID string
   * @param substituteName
   *          Name for the substitute
   * @param substituteId
   *          Substitute product ID string
   * @return JSON object returned by SubstituteProcess.doExecute method
   * @throws JSONException
   * @throws NoSuchMethodException
   * @throws SecurityException
   * @throws IllegalAccessException
   * @throws IllegalArgumentException
   * @throws InvocationTargetException
   * @see SubstituteProcess ActionHandler
   */
  public static JSONObject addSubstitute(final String productID, final String substituteName,
      final String substituteId) throws JSONException, NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException {

    JSONObject content = new JSONObject();

    content.put("M_Product_ID", productID);
    JSONObject params = new JSONObject();
    params.put("Substitute_ID", substituteId);
    params.put("Name", substituteName);
    content.put("_params", params);

    JSONObject result = new SubstituteProcess() {
      public JSONObject publicExecute(Map<String, Object> _parameters, String _content) {
        return super.doExecute(_parameters, _content);
      }
    }.publicExecute(new HashMap<String, Object>(), content.toString());

    OBDal.getInstance().commitAndClose();
    return result;

  }

  /**
   * General function to create a BOM version
   * 
   * @param productID
   *          Product ID string
   * @param bomVersionID
   *          BOM Version ID string
   * @param deleteOld
   *          Set true to remove old material of the BOM Version
   * @return JSON object returned by SubstituteProcess.doExecute method
   * @throws JSONException
   * @throws NoSuchMethodException
   * @throws SecurityException
   * @throws IllegalAccessException
   * @throws IllegalArgumentException
   * @throws InvocationTargetException
   * @see CreateBOMVersion ActionHandler
   */
  public static JSONObject createBOMVersion(final String productID, final String bomVersionID,
      boolean deleteOld) throws JSONException, NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException {

    JSONObject content = new JSONObject();
    content.put("M_Product_ID", productID);
    content.put("Obpbomv_Bom_Version_ID", bomVersionID);

    JSONObject params = new JSONObject();
    params.put("deleteOld", deleteOld);
    content.put("_params", params);

    JSONObject result = new CreateBOMVersion() {
      public JSONObject publicExecute(Map<String, Object> _parameters, String _content) {
        return super.doExecute(_parameters, _content);
      }
    }.publicExecute(new HashMap<String, Object>(), content.toString());

    OBDal.getInstance().commitAndClose();
    return result;

  }

  /**
   * General function to verify BOM versions of a product
   * 
   * @param productID
   *          Product ID string
   * @param bomVersionID
   *          BOM Version ID
   * @return JSON object returned by VerifyBillOfMaterials.doExecute method
   * @throws JSONException
   * @throws NoSuchMethodException
   * @throws SecurityException
   * @throws IllegalAccessException
   * @throws IllegalArgumentException
   * @throws InvocationTargetException
   * @see VerifyBomVersionProcesss ActionHandler
   */
  public static JSONObject verifyBOMVersion(final String productID, final String bomVersionID)
      throws JSONException, NoSuchMethodException, SecurityException, IllegalAccessException,
      IllegalArgumentException, InvocationTargetException {

    JSONObject content = new JSONObject();

    content.put("M_Product_ID", productID);
    content.put("Obpbomv_Bom_Version_ID", bomVersionID);

    JSONObject result = new VerifyBomVersionProcess() {
      public JSONObject publicExecute(Map<String, Object> _parameters, String _content) {
        return super.doExecute(_parameters, _content);
      }
    }.publicExecute(new HashMap<String, Object>(), content.toString());

    OBDal.getInstance().commitAndClose();
    return result;

  }

  public static BOMVersion createBOMVersionRecord(Product product, final String name) {

    BOMVersion bomVersion = OBProvider.getInstance().get(BOMVersion.class);
    bomVersion.setProduct(product);
    bomVersion.setName(name);
    Date now = Calendar.getInstance().getTime();
    bomVersion.setValidFromDate(now);
    bomVersion.setValidToDate(now);
    OBDal.getInstance().save(bomVersion);
    OBDal.getInstance().flush();
    OBDal.getInstance().commitAndClose();
    bomVersion = OBDal.getInstance().get(BOMVersion.class, bomVersion.getId());
    return bomVersion;
  }
}
