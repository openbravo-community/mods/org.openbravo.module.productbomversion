/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2018-2022 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.module.productbomversion.test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.weld.test.WeldBaseTest;
import org.openbravo.client.kernel.RequestContext;
import org.openbravo.dal.core.DalUtil;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductBOM;
import org.openbravo.module.productbomversion.test.api.BOMUtils;

/**
 * Tests AddSubstituteTest action handler .<br>
 * 
 */
public class AddSubstituteTest extends WeldBaseTest {

  public static final String BOMA_PRODUCT_ID = "4028E6C72959682B01295ADC1F580233";

  public static final String FINAL_GOOD_A_ID = "4028E6C72959682B01295ADC1D07022A";
  public static final String FINAL_GOOD_B_ID = "4028E6C72959682B01295ADC1DC2022E";

  @Before
  public void setUp() throws Exception {
    super.setUp();
    BOMVersionOBContext.setTestUserContext();
    VariablesSecureApp vars = null;
    vars = new VariablesSecureApp(OBContext.getOBContext().getUser().getId(),
        OBContext.getOBContext().getCurrentClient().getId(),
        OBContext.getOBContext().getCurrentOrganization().getId(),
        OBContext.getOBContext().getRole().getId(),
        OBContext.getOBContext().getLanguage().getLanguage());
    RequestContext.get().setVariableSecureApp(vars);
  }

  /**
   * A test to verify a cycle creation when add substitute
   */
  @Test
  public void testOBPBOMV130() throws JSONException, NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException {

    Product productWithBomA5 = null;
    Product productWithBomB5 = null;
    Product productWithBomC5 = null;
    Product simpleMaterialB5 = null;

    try {

      Product bomAsample = OBDal.getInstance().get(Product.class, BOMA_PRODUCT_ID);

      // Create BOM-A5
      productWithBomA5 = (Product) DalUtil.copy(bomAsample, false);

      // Change attributes
      productWithBomA5.setValue(Product.PROPERTY_SEARCHKEY, "BOM-A5");
      productWithBomA5.setValue(Product.PROPERTY_NAME, "BOM-A5");
      productWithBomA5.setUPCEAN(null);
      productWithBomA5.setObpbomvIssubstitute(true);
      OBDal.getInstance().save(productWithBomA5);

      assertThat(productWithBomA5.isBOMVerified(), equalTo(Boolean.FALSE));

      // Add non BOM material
      BOMUtils.createMaterial(10L, productWithBomA5,
          OBDal.getInstance().get(Product.class, FINAL_GOOD_A_ID), BigDecimal.ONE, BigDecimal.ONE);

      // Create simple material product
      Product simpleMaterial = OBDal.getInstance().get(Product.class, FINAL_GOOD_B_ID);
      simpleMaterialB5 = (Product) DalUtil.copy(simpleMaterial, false);

      // Change attributes
      simpleMaterialB5.setValue(Product.PROPERTY_SEARCHKEY, "SimpleMaterial-B5");
      simpleMaterialB5.setValue(Product.PROPERTY_NAME, "SimpleMaterial-B5");
      simpleMaterialB5.setUPCEAN(null);
      OBDal.getInstance().save(simpleMaterialB5);
      OBDal.getInstance().flush();

      // Create BOM-C5
      productWithBomC5 = (Product) DalUtil.copy(bomAsample, false);

      // Change attributes
      productWithBomC5.setValue(Product.PROPERTY_SEARCHKEY, "BOM-C5");
      productWithBomC5.setValue(Product.PROPERTY_NAME, "BOM-C5");
      productWithBomC5.setUPCEAN(null);
      OBDal.getInstance().save(productWithBomC5);

      assertThat(productWithBomC5.isBOMVerified(), equalTo(Boolean.FALSE));

      // Add simpleMaterialB5 as material of productWithBomB5
      BOMUtils.createMaterial(10L, productWithBomC5, simpleMaterialB5, BigDecimal.ONE,
          BigDecimal.ONE);

      // Create BOM-B5
      productWithBomB5 = (Product) DalUtil.copy(bomAsample, false);

      // Change attributes
      productWithBomB5.setValue(Product.PROPERTY_SEARCHKEY, "BOM-B5");
      productWithBomB5.setValue(Product.PROPERTY_NAME, "BOM-B5");
      productWithBomB5.setUPCEAN(null);
      OBDal.getInstance().save(productWithBomB5);

      assertThat(productWithBomB5.isBOMVerified(), equalTo(Boolean.FALSE));

      // Add simpleMaterialB5 as material of productWithBomB5
      BOMUtils.createMaterial(10L, productWithBomB5, simpleMaterialB5, BigDecimal.ONE,
          BigDecimal.ONE);

      // Add BOM-B5 as material of BOM-A5
      BOMUtils.createMaterial(20L, productWithBomA5, productWithBomB5, BigDecimal.ONE,
          BigDecimal.ONE);

      // Check cycle when add substitute
      JSONObject message = BOMUtils
          .addSubstitute(simpleMaterialB5.getId(), "S5", productWithBomA5.getId())
          .getJSONArray("responseActions")
          .getJSONObject(0)
          .getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "error",
          "BOM Tree Contains a Cycle Please Verify BOM");
      simpleMaterialB5 = OBDal.getInstance().get(Product.class, simpleMaterialB5.getId());
      assertThat(simpleMaterialB5.isBOMVerified(), equalTo(Boolean.FALSE));

    } finally {
      if (productWithBomA5 != null && productWithBomB5 != null && productWithBomC5 != null
          && simpleMaterialB5 != null) {

        BOMUtils.recordsCleanUp(ProductBOM.class,
            ProductBOM.PROPERTY_PRODUCT + "='" + productWithBomC5.getId() + "'");
        BOMUtils.recordsCleanUp(ProductBOM.class,
            ProductBOM.PROPERTY_PRODUCT + "='" + productWithBomA5.getId() + "'");
        BOMUtils.recordsCleanUp(ProductBOM.class,
            ProductBOM.PROPERTY_PRODUCT + "='" + productWithBomB5.getId() + "'");
        productWithBomB5 = OBDal.getInstance().get(Product.class, productWithBomB5.getId());
        BOMUtils.recordsCleanUp(Product.class,
            Product.PROPERTY_ID + "='" + productWithBomB5.getId() + "'");
        simpleMaterialB5 = OBDal.getInstance().get(Product.class, simpleMaterialB5.getId());
        BOMUtils.recordsCleanUp(Product.class,
            Product.PROPERTY_ID + "='" + simpleMaterialB5.getId() + "'");
        productWithBomA5 = OBDal.getInstance().get(Product.class, productWithBomA5.getId());
        BOMUtils.recordsCleanUp(Product.class,
            Product.PROPERTY_ID + "='" + productWithBomA5.getId() + "'");
        productWithBomC5 = OBDal.getInstance().get(Product.class, productWithBomC5.getId());
        BOMUtils.recordsCleanUp(Product.class,
            Product.PROPERTY_ID + "='" + productWithBomC5.getId() + "'");
      }
    }
  }

  /**
   * Test the cycle detection of the Substitute process on existent cycles
   */
  @Test
  public void testOBPBOMV140() throws JSONException, NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException {

    Product productWithBomB3 = null;
    Product productWithBomA3 = null;
    try {
      Product bomAsample = OBDal.getInstance().get(Product.class, BOMA_PRODUCT_ID);

      // Create BOM-A3
      productWithBomA3 = (Product) DalUtil.copy(bomAsample, false);

      // Change attributes
      productWithBomA3.setValue(Product.PROPERTY_SEARCHKEY, "BOM-A3");
      productWithBomA3.setValue(Product.PROPERTY_NAME, "BOM-A3");
      productWithBomA3.setUPCEAN(null);
      OBDal.getInstance().save(productWithBomA3);

      assertThat(productWithBomA3.isBOMVerified(), equalTo(Boolean.FALSE));

      // Create material
      BOMUtils.createMaterial(10L, productWithBomA3,
          OBDal.getInstance().get(Product.class, FINAL_GOOD_A_ID), BigDecimal.ONE, BigDecimal.ONE);

      // Verify BOM-A3
      JSONObject message = BOMUtils.verifyBOM(productWithBomA3.getId())
          .getJSONArray("responseActions")
          .getJSONObject(0)
          .getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "success", "Process completed successfully");

      // reload the properties
      productWithBomA3 = OBDal.getInstance().get(Product.class, productWithBomA3.getId());
      assertThat(productWithBomA3.isBOMVerified(), equalTo(Boolean.TRUE));

      // Create BOM-B3
      productWithBomB3 = (Product) DalUtil.copy(bomAsample, false);

      // Change attributes
      productWithBomB3.setValue(Product.PROPERTY_SEARCHKEY, "BOM-B3");
      productWithBomB3.setValue(Product.PROPERTY_NAME, "BOM-B3");
      productWithBomB3.setUPCEAN(null);
      OBDal.getInstance().save(productWithBomB3);

      assertThat(productWithBomB3.isBOMVerified(), equalTo(Boolean.FALSE));

      // Add BOM-A3 as material of BOM-B3
      BOMUtils.createMaterial(10L, productWithBomB3, productWithBomA3, BigDecimal.ONE,
          BigDecimal.ONE);

      // Verify BOM-B3
      message = BOMUtils.verifyBOM(productWithBomB3.getId())
          .getJSONArray("responseActions")
          .getJSONObject(0)
          .getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "success", "Process completed successfully");

      // reload the properties
      productWithBomB3 = OBDal.getInstance().get(Product.class, productWithBomB3.getId());
      assertThat(productWithBomB3.isBOMVerified(), equalTo(Boolean.TRUE));

      // Add BOM-B3 as material of BOM-A3 (a cycle)
      productWithBomA3 = OBDal.getInstance().get(Product.class, productWithBomA3.getId());
      BOMUtils.createMaterial(10L, productWithBomA3, productWithBomB3, BigDecimal.ONE,
          BigDecimal.ONE);

      // Checks that exists a cycle BOM-A3-> BOM-B3 -> BOM-A3
      message = BOMUtils.verifyBOM(productWithBomA3.getId())
          .getJSONArray("responseActions")
          .getJSONObject(0)
          .getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "error",
          "BOM Tree Contains a Cycle Please Verify BOM");

      // reload the properties
      productWithBomA3 = OBDal.getInstance().get(Product.class, productWithBomA3.getId());
      assertThat(productWithBomA3.isBOMVerified(), equalTo(Boolean.FALSE));

      // Checks that exists a cycle BOM-B3-> BOM-A3 -> BOM-B3
      // TODO: This must be checked because productWithBomB3->productWithBomA3->productWithBomB3
      // cycle is also formed. Commenting because at this point Verify BOM button is not visible.
      // message = BOMUtils.verifyBOM(productWithBomB3.getId()).getJSONArray("responseActions")
      // .getJSONObject(0).getJSONObject("showMsgInProcessView");
      // BOMVersionTestSuite.assertResponse(message, "error",
      // "BOM Tree Contains a Cycle Please Verify BOM");
      // // reload the properties
      // productWithBomB3 = OBDal.getInstance().get(Product.class, productWithBomB3.getId());
      // assertThat(productWithBomB3.isBOMVerified(), equalTo(Boolean.FALSE));

      // Adding a substitute to either BOM-A3 or BOM-B3 must detect the cycle too
      message = BOMUtils.addSubstitute(productWithBomB3.getId(), "S3", FINAL_GOOD_B_ID)
          .getJSONArray("responseActions")
          .getJSONObject(0)
          .getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "error",
          "BOM Tree Contains a Cycle Please Verify BOM");

    } finally {
      if (productWithBomA3 != null && productWithBomB3 != null) {
        BOMUtils.recordsCleanUp(ProductBOM.class,
            ProductBOM.PROPERTY_PRODUCT + "='" + productWithBomA3.getId() + "'");
        BOMUtils.recordsCleanUp(ProductBOM.class,
            ProductBOM.PROPERTY_PRODUCT + "='" + productWithBomB3.getId() + "'");
        productWithBomA3 = OBDal.getInstance().get(Product.class, productWithBomA3.getId());
        BOMUtils.recordsCleanUp(Product.class,
            Product.PROPERTY_ID + "='" + productWithBomA3.getId() + "'");
        productWithBomB3 = OBDal.getInstance().get(Product.class, productWithBomB3.getId());
        BOMUtils.recordsCleanUp(Product.class,
            Product.PROPERTY_ID + "='" + productWithBomB3.getId() + "'");
      }
    }
  }
}
