/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2018 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.module.productbomversion.test;

import org.openbravo.dal.core.OBContext;

/**
 * Utility class for setup Openbravo context
 * 
 *
 */
public class BOMVersionOBContext {

  /**
   * Record ID of the QA Test client
   */
  protected static final String QA_TEST_CLIENT_ID = "4028E6C72959682B01295A070852010D";

  /**
   * Record ID of the Main organization of QA Test client
   */
  protected static final String QA_TEST_ORG_ID = "43D590B4814049C6B85C6545E8264E37";

  /**
   * Record ID of the "Admin" user of QA Test client
   */
  protected static final String QA_TEST_ADMIN_USER_ID = "4028E6C72959682B01295A0735CB0120";

  /**
   * Record ID of the "Admin" role of QA Test client
   */
  protected static final String QA_TEST_ADMIN_ROLE_ID = "4028E6C72959682B01295A071429011E";

  protected static void setTestUserContext() {
    OBContext.setOBContext(QA_TEST_ADMIN_USER_ID, QA_TEST_ADMIN_ROLE_ID, QA_TEST_CLIENT_ID,
        QA_TEST_ORG_ID);
  }

}
