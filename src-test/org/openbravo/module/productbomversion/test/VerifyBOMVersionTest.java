/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2018-2021 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.module.productbomversion.test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.List;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.weld.test.WeldBaseTest;
import org.openbravo.client.kernel.RequestContext;
import org.openbravo.dal.core.DalUtil;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductBOM;
import org.openbravo.module.productbomversion.BOMVersion;
import org.openbravo.module.productbomversion.BOMVersionProduct;
import org.openbravo.module.productbomversion.test.api.BOMUtils;

/**
 * Tests VerifyBOMVersionProcess action handler .<br>
 * 
 */
public class VerifyBOMVersionTest extends WeldBaseTest {

  public static final String BOMA_PRODUCT_ID = "4028E6C72959682B01295ADC1F580233";

  public static final String FINAL_GOOD_A_ID = "4028E6C72959682B01295ADC1D07022A";
  public static final String FINAL_GOOD_B_ID = "4028E6C72959682B01295ADC1DC2022E";

  @Before
  public void setUp() throws Exception {
    super.setUp();
    BOMVersionOBContext.setTestUserContext();
    VariablesSecureApp vars = null;
    vars = new VariablesSecureApp(OBContext.getOBContext().getUser().getId(), OBContext
        .getOBContext().getCurrentClient().getId(), OBContext.getOBContext()
        .getCurrentOrganization().getId(), OBContext.getOBContext().getRole().getId(), OBContext
        .getOBContext().getLanguage().getLanguage());
    RequestContext.get().setVariableSecureApp(vars);
  }

  /**
   * A test to verify a product with empty BOM Version
   */
  @Test
  public void testOBPBOMV150() throws JSONException, NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException {

    Product productWithEmptyBom = null;
    try {
      Product bomAsample = OBDal.getInstance().get(Product.class, BOMA_PRODUCT_ID);

      // Create empty BOM
      productWithEmptyBom = (Product) DalUtil.copy(bomAsample, false);
      // Change attributes
      productWithEmptyBom.setValue(Product.PROPERTY_SEARCHKEY, "BOM-A15");
      productWithEmptyBom.setValue(Product.PROPERTY_NAME, "BOM-A15");
      productWithEmptyBom.setUPCEAN(null);
      OBDal.getInstance().save(productWithEmptyBom);
      OBDal.getInstance().flush();
      assertThat(productWithEmptyBom.isBOMVerified(), equalTo(Boolean.FALSE));

      BOMVersion bomVersion = BOMUtils.createBOMVersionRecord(productWithEmptyBom, "V1.0");

      assertThat(bomVersion.isObpbomvBomverified(), equalTo(Boolean.FALSE));

      JSONObject message = BOMUtils
          .createBOMVersion(productWithEmptyBom.getId(), bomVersion.getId(), false)
          .getJSONArray("responseActions").getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "success", "Process completed successfully");

      message = BOMUtils.verifyBOMVersion(productWithEmptyBom.getId(), bomVersion.getId())
          .getJSONArray("responseActions").getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "error",
          "Verify that you have included one or more products in tab Product BOM Version");

      // reload the properties
      bomVersion = OBDal.getInstance().get(BOMVersion.class, bomVersion.getId());

      assertThat(bomVersion.isObpbomvBomverified(), equalTo(Boolean.FALSE));
    } finally {
      if (productWithEmptyBom != null) {
        BOMUtils.recordsCleanUp(Product.class,
            Product.PROPERTY_ID + "='" + productWithEmptyBom.getId() + "'");
      }
    }
  }

  /**
   * A test to verify creation of product BOM version with material
   */
  @Test
  public void testOBPBOMV160() throws JSONException, NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException {

    Product productWithBom = null;
    try {
      Product bomAsample = OBDal.getInstance().get(Product.class, BOMA_PRODUCT_ID);
      productWithBom = (Product) DalUtil.copy(bomAsample, false);
      // Change attributes
      productWithBom.setValue(Product.PROPERTY_SEARCHKEY, "BOM-A16");
      productWithBom.setValue(Product.PROPERTY_NAME, "BOM-A16");
      productWithBom.setUPCEAN(null);

      assertThat(productWithBom.isBOMVerified(), equalTo(Boolean.FALSE));
      OBDal.getInstance().save(productWithBom);
      OBDal.getInstance().flush();

      // Create materials
      ProductBOM materiaA = BOMUtils.createMaterial(10L, productWithBom,
          OBDal.getInstance().get(Product.class, FINAL_GOOD_A_ID), BigDecimal.ONE, BigDecimal.ONE);

      ProductBOM materiaB = BOMUtils.createMaterial(20L, productWithBom,
          OBDal.getInstance().get(Product.class, FINAL_GOOD_B_ID), BigDecimal.ONE, BigDecimal.ONE);

      ProductBOM materiaA1 = BOMUtils.createMaterial(30L, productWithBom,
          OBDal.getInstance().get(Product.class, FINAL_GOOD_A_ID), BigDecimal.ONE, BigDecimal.ONE);

      JSONObject message = BOMUtils.verifyBOM(productWithBom.getId())
          .getJSONArray("responseActions").getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "success", "Process completed successfully");

      // reload the properties
      productWithBom = OBDal.getInstance().get(Product.class, productWithBom.getId());
      assertThat(productWithBom.isBOMVerified(), equalTo(Boolean.TRUE));

      BOMVersion bomVersion = BOMUtils.createBOMVersionRecord(productWithBom, "V1.0");
      assertThat(bomVersion.isObpbomvBomverified(), equalTo(Boolean.FALSE));

      // Create BOM version with deleteOld = false
      message = BOMUtils.createBOMVersion(productWithBom.getId(), bomVersion.getId(), false)
          .getJSONArray("responseActions").getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "success", "Process completed successfully");

      bomVersion = OBDal.getInstance().get(BOMVersion.class, bomVersion.getId());
      List<BOMVersionProduct> productBOMVersionList = bomVersion.getOBPBOMVBOMVersionProductList();

      assertThat(productBOMVersionList.size(), equalTo(3));
      assertThat(materiaA.getBOMProduct().getId(), equalTo(productBOMVersionList.get(0)
          .getProduct().getId()));
      assertThat(materiaB.getBOMProduct().getId(), equalTo(productBOMVersionList.get(1)
          .getProduct().getId()));
      assertThat(materiaA1.getBOMProduct().getId(), equalTo(productBOMVersionList.get(2)
          .getProduct().getId()));
      assertThat(bomVersion.isObpbomvBomverified(), equalTo(Boolean.FALSE));

      // Verify BOM Version
      message = BOMUtils.verifyBOMVersion(productWithBom.getId(), bomVersion.getId())
          .getJSONArray("responseActions").getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "success", "Process completed successfully");
      bomVersion = OBDal.getInstance().get(BOMVersion.class, bomVersion.getId());
      assertThat(bomVersion.isObpbomvBomverified(), equalTo(Boolean.TRUE));

      // Create BOM version with deleteOld = true
      message = BOMUtils.createBOMVersion(productWithBom.getId(), bomVersion.getId(), true)
          .getJSONArray("responseActions").getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "success", "Process completed successfully");

      bomVersion = OBDal.getInstance().get(BOMVersion.class, bomVersion.getId());
      productBOMVersionList = bomVersion.getOBPBOMVBOMVersionProductList();

      assertThat(productBOMVersionList.size(), equalTo(3));
      assertThat(materiaA.getBOMProduct().getId(), equalTo(productBOMVersionList.get(0)
          .getProduct().getId()));
      assertThat(materiaB.getBOMProduct().getId(), equalTo(productBOMVersionList.get(1)
          .getProduct().getId()));
      assertThat(materiaA1.getBOMProduct().getId(), equalTo(productBOMVersionList.get(2)
          .getProduct().getId()));
      assertThat(bomVersion.isObpbomvBomverified(), equalTo(Boolean.FALSE));

      // Verify BOM Version
      message = BOMUtils.verifyBOMVersion(productWithBom.getId(), bomVersion.getId())
          .getJSONArray("responseActions").getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "success", "Process completed successfully");
      bomVersion = OBDal.getInstance().get(BOMVersion.class, bomVersion.getId());
      assertThat(bomVersion.isObpbomvBomverified(), equalTo(Boolean.TRUE));

    } finally {

      if (productWithBom != null) {
        BOMUtils.recordsCleanUp(Product.class, Product.PROPERTY_ID + "='" + productWithBom.getId()
            + "'");
      }
    }
  }

  /**
   * A test to verify the creation of BOM Versions with materials having negative BOM quantity
   */
  @Test
  public void testOBPBOMV170() throws JSONException, NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException {

    Product productWithBom = null;
    try {
      Product bomAsample = OBDal.getInstance().get(Product.class, BOMA_PRODUCT_ID);
      productWithBom = (Product) DalUtil.copy(bomAsample, false);
      // Change attributes
      productWithBom.setValue(Product.PROPERTY_SEARCHKEY, "BOM-A17");
      productWithBom.setValue(Product.PROPERTY_NAME, "BOM-A17");
      productWithBom.setUPCEAN(null);
      assertThat(productWithBom.isBOMVerified(), equalTo(Boolean.FALSE));
      OBDal.getInstance().save(productWithBom);
      OBDal.getInstance().flush();

      // Create materials
      ProductBOM materialA = BOMUtils.createMaterial(10L, productWithBom,
          OBDal.getInstance().get(Product.class, FINAL_GOOD_A_ID), BigDecimal.ONE, BigDecimal.ONE);

      ProductBOM materialB = BOMUtils.createMaterial(20L, productWithBom,
          OBDal.getInstance().get(Product.class, FINAL_GOOD_B_ID), BigDecimal.ONE.negate(),
          BigDecimal.ONE);

      JSONObject message = BOMUtils.verifyBOM(productWithBom.getId())
          .getJSONArray("responseActions").getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "error",
          "It is not allowed to insert negative quantities in BOM Products");

      // reload the properties
      productWithBom = OBDal.getInstance().get(Product.class, productWithBom.getId());
      assertThat(productWithBom.isBOMVerified(), equalTo(Boolean.FALSE));

      BOMVersion bomVersion = BOMUtils.createBOMVersionRecord(productWithBom, "V1.0");
      assertThat(bomVersion.isObpbomvBomverified(), equalTo(Boolean.FALSE));

      // Create BOM version with deleteOld = false
      message = BOMUtils.createBOMVersion(productWithBom.getId(), bomVersion.getId(), false)
          .getJSONArray("responseActions").getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "success", "Process completed successfully");

      bomVersion = OBDal.getInstance().get(BOMVersion.class, bomVersion.getId());
      List<BOMVersionProduct> productBOMVersionList = bomVersion.getOBPBOMVBOMVersionProductList();

      assertThat(productBOMVersionList.size(), equalTo(2));
      assertThat(materialA.getBOMProduct().getId(), equalTo(productBOMVersionList.get(0)
          .getProduct().getId()));
      assertThat(materialB.getBOMProduct().getId(), equalTo(productBOMVersionList.get(1)
          .getProduct().getId()));
      assertThat(materialB.getBOMQuantity(), equalTo(productBOMVersionList.get(1).getBOMQuantity()));
      assertThat(bomVersion.isObpbomvBomverified(), equalTo(Boolean.FALSE));

      // Verify BOM Version
      message = BOMUtils.verifyBOMVersion(productWithBom.getId(), bomVersion.getId())
          .getJSONArray("responseActions").getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "error",
          "It is not allowed to insert negative quantities in BOM Products");

      bomVersion = OBDal.getInstance().get(BOMVersion.class, bomVersion.getId());
      assertThat(bomVersion.isObpbomvBomverified(), equalTo(Boolean.FALSE));

      // Update original BOM Materials
      materialB = OBDal.getInstance().get(ProductBOM.class, materialB.getId());
      materialB.setBOMQuantity(BigDecimal.ONE);
      productWithBom = OBDal.getInstance().get(Product.class, productWithBom.getId());
      ProductBOM materialA1 = BOMUtils.createMaterial(30L, productWithBom,
          OBDal.getInstance().get(Product.class, FINAL_GOOD_A_ID), BigDecimal.ONE, BigDecimal.ONE);

      // Create BOM version with deleteOld = false
      productWithBom = OBDal.getInstance().get(Product.class, productWithBom.getId());
      message = BOMUtils.createBOMVersion(productWithBom.getId(), bomVersion.getId(), false)
          .getJSONArray("responseActions").getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "success", "Process completed successfully");

      bomVersion = OBDal.getInstance().get(BOMVersion.class, bomVersion.getId());
      productBOMVersionList = bomVersion.getOBPBOMVBOMVersionProductList();

      assertThat(productBOMVersionList.size(), equalTo(3));
      assertThat(materialA.getBOMProduct().getId(), equalTo(productBOMVersionList.get(0)
          .getProduct().getId()));
      assertThat(materialB.getBOMProduct().getId(), equalTo(productBOMVersionList.get(1)
          .getProduct().getId()));
      assertThat(materialA1.getBOMProduct().getId(), equalTo(productBOMVersionList.get(2)
          .getProduct().getId()));
      assertThat(bomVersion.isObpbomvBomverified(), equalTo(Boolean.FALSE));

      // Verify BOM Version
      message = BOMUtils.verifyBOMVersion(productWithBom.getId(), bomVersion.getId())
          .getJSONArray("responseActions").getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "success", "Process completed successfully");

      bomVersion = OBDal.getInstance().get(BOMVersion.class, bomVersion.getId());
      assertThat(bomVersion.isObpbomvBomverified(), equalTo(Boolean.TRUE));

    } finally {

      if (productWithBom != null) {
        BOMUtils.recordsCleanUp(Product.class, Product.PROPERTY_ID + "='" + productWithBom.getId()
            + "'");
      }
    }
  }

  /**
   * A test to check that an error show up when it’s created a Product with BOM Version that contain
   * it self as material
   */
  @Test
  public void testOBPBOMV180() throws JSONException, NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException {

    Product productWithBom = null;
    BOMVersion bomVersion = null;
    try {
      Product bomAsample = OBDal.getInstance().get(Product.class, BOMA_PRODUCT_ID);
      productWithBom = (Product) DalUtil.copy(bomAsample, false);
      // Change attributes
      productWithBom.setValue(Product.PROPERTY_SEARCHKEY, "BOM-A18");
      productWithBom.setValue(Product.PROPERTY_NAME, "BOM-A18");
      productWithBom.setUPCEAN(null);
      assertThat(productWithBom.isBOMVerified(), equalTo(Boolean.FALSE));
      OBDal.getInstance().save(productWithBom);

      // Create material
      BOMUtils.createMaterial(10L, productWithBom,
          OBDal.getInstance().get(Product.class, FINAL_GOOD_A_ID), BigDecimal.ONE, BigDecimal.ONE);

      // Check 1st Verify BOM
      JSONObject message = BOMUtils.verifyBOM(productWithBom.getId())
          .getJSONArray("responseActions").getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "success", "Process completed successfully");

      // reload the properties
      productWithBom = OBDal.getInstance().get(Product.class, productWithBom.getId());
      assertThat(productWithBom.isBOMVerified(), equalTo(Boolean.TRUE));

      // Create it self as material
      BOMUtils.createMaterial(20L, productWithBom, productWithBom, BigDecimal.ONE, BigDecimal.ONE);

      message = BOMUtils.verifyBOM(productWithBom.getId()).getJSONArray("responseActions")
          .getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "error",
          "BOM Tree Contains a Cycle Please Verify BOM");

      // reload the properties
      productWithBom = OBDal.getInstance().get(Product.class, productWithBom.getId());
      assertThat(productWithBom.isBOMVerified(), equalTo(Boolean.FALSE));

      bomVersion = BOMUtils.createBOMVersionRecord(productWithBom, "V1.0");
      assertThat(bomVersion.isObpbomvBomverified(), equalTo(Boolean.FALSE));

      // Create BOM version with deleteOld = false
      message = BOMUtils.createBOMVersion(productWithBom.getId(), bomVersion.getId(), false)
          .getJSONArray("responseActions").getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "success", "Process completed successfully");

      bomVersion = OBDal.getInstance().get(BOMVersion.class, bomVersion.getId());
      assertThat(bomVersion.isObpbomvBomverified(), equalTo(Boolean.FALSE));

      // Verify BOM Version
      message = BOMUtils.verifyBOMVersion(productWithBom.getId(), bomVersion.getId())
          .getJSONArray("responseActions").getJSONObject(0).getJSONObject("showMsgInProcessView");
      BOMVersionTestSuite.assertResponse(message, "error",
          "BOM Tree Contains a Cycle Please Verify BOM");

      bomVersion = OBDal.getInstance().get(BOMVersion.class, bomVersion.getId());
      assertThat(bomVersion.isObpbomvBomverified(), equalTo(Boolean.FALSE));

    } finally {
      if (productWithBom != null && bomVersion != null) {

        BOMUtils.recordsCleanUp(BOMVersionProduct.class,
            BOMVersionProduct.PROPERTY_OBPBOMVBOMVERSION + "='" + bomVersion.getId() + "'");

        BOMUtils.recordsCleanUp(ProductBOM.class, ProductBOM.PROPERTY_PRODUCT + "='"
            + productWithBom.getId() + "'");
        BOMUtils.recordsCleanUp(Product.class, Product.PROPERTY_ID + "='" + productWithBom.getId()
            + "'");
      }
    }
  }
}
