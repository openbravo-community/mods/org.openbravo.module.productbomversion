/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2018-2021 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.module.productbomversion.test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Tests Suite project<br>
 * 
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ VerifyBOMTest.class, AddSubstituteTest.class, VerifyBOMVersionTest.class })
public class BOMVersionTestSuite {
  public static void assertResponse(final JSONObject message, final String msgType,
      final String msgText) throws JSONException {
    assertThat(message.getString("msgType"), equalTo(msgType));
    assertThat(message.getString("msgText"), equalTo(msgText));

  }
}
