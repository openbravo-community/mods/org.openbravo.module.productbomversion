/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.module.productbomversion.event;

import javax.enterprise.event.Observes;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.module.productbomversion.BOMVersion;

public class ProductBOMVersionEventHandler extends EntityPersistenceEventObserver {
  private static Entity[] entities = { ModelProvider.getInstance()
      .getEntity(BOMVersion.ENTITY_NAME) };
  protected Logger logger = Logger.getLogger(this.getClass());

  @Override
  protected Entity[] getObservedEntities() {
    return entities;
  }

  public void onUpdate(@Observes EntityUpdateEvent event) {
    if (!isValidEvent(event)) {
      return;
    }
    BOMVersion productBomVersion = (BOMVersion) event.getTargetInstance();
    validateBOMVersionDates(productBomVersion, true);
  }

  public void onSave(@Observes EntityNewEvent event) {
    if (!isValidEvent(event)) {
      return;
    }
    BOMVersion productBomVersion = (BOMVersion) event.getTargetInstance();
    validateBOMVersionDates(productBomVersion, false);
  }

  private void validateBOMVersionDates(BOMVersion productBomVersion, boolean updating) {
    if (productBomVersion.getValidToDate().before(productBomVersion.getValidFromDate())) {
      throw new OBException("@OBPBOMV_InvalidDateRange@");
    }

    OBCriteria<BOMVersion> criteria = OBDal.getInstance().createCriteria(BOMVersion.class);
    criteria.add(Restrictions.eq(BOMVersion.PROPERTY_PRODUCT, productBomVersion.getProduct()));
    criteria.add(Restrictions.eq(BOMVersion.PROPERTY_VALIDFROMDATE,
        productBomVersion.getValidFromDate()));
    if (updating) {
      criteria.add(Restrictions.not(Restrictions.idEq(productBomVersion.getId())));
    }
    criteria.setFilterOnActive(false);
    criteria.setMaxResults(1);
    if (criteria.uniqueResult() != null) {
      throw new OBException("@OBPBOMV_ExistValidFromDate@");
    }

  }
}
