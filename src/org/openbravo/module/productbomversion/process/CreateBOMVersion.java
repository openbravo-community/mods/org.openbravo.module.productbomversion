/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.module.productbomversion.process;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.client.application.process.ResponseActionsBuilder.MessageType;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBDao;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductBOM;
import org.openbravo.module.productbomversion.BOMVersion;
import org.openbravo.module.productbomversion.BOMVersionProduct;
import org.openbravo.service.db.DbUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateBOMVersion extends BaseProcessActionHandler {
  private static final Logger log = LoggerFactory.getLogger(CreateBOMVersion.class);

  @Override
  protected JSONObject doExecute(Map<String, Object> parameters, String content) {
    OBContext.setAdminMode(true);
    try {
      JSONObject jsonData = new JSONObject(content);
      String productId = jsonData.getString("M_Product_ID");
      String bomVersionId = jsonData.getString("Obpbomv_Bom_Version_ID");
      JSONObject jsonparams = jsonData.getJSONObject("_params");
      final String deleteOld = jsonparams.getString("deleteOld");
      Product product = OBDal.getInstance().get(Product.class, productId);
      BOMVersion bomVersion = OBDal.getInstance().get(BOMVersion.class, bomVersionId);

      if (deleteOld.equals("true")) {
        List<String> pdsIds = OBDao.getIDListFromOBObject(bomVersion
            .getOBPBOMVBOMVersionProductList());
        for (String strPDSId : pdsIds) {
          BOMVersionProduct pbv = OBDal.getInstance().get(BOMVersionProduct.class, strPDSId);
          OBDal.getInstance().remove(pbv);
          bomVersion.getOBPBOMVBOMVersionProductList().remove(pbv);
        }
        for (ProductBOM bom : product.getProductBOMList()) {
          BOMVersionProduct bomVersionProduct = OBProvider.getInstance().get(
              BOMVersionProduct.class);
          bomVersionProduct.setOrganization(bom.getOrganization());
          bomVersionProduct.setProduct(bom.getBOMProduct());
          bomVersionProduct.setObpbomvBomVersion(bomVersion);
          bomVersionProduct.setBOMQuantity(bom.getBOMQuantity());
          bomVersionProduct.setLineNo(bom.getLineNo());
          bomVersionProduct.setDescription(bom.getDescription());
          bomVersionProduct.setCreatedBy(bom.getCreatedBy());
          bomVersionProduct.setUpdatedBy(bom.getUpdatedBy());
          OBDal.getInstance().save(bomVersionProduct);
        }
      } else {
        Set<String> updatedMaterials = new TreeSet<>();
        for (ProductBOM bom : product.getProductBOMList()) {
          BOMVersionProduct bomVersionProduct = null;
          if (!updatedMaterials.contains(bom.getBOMProduct().getId())) {
            OBCriteria<BOMVersionProduct> obCriteria = OBDal.getInstance().createCriteria(
                BOMVersionProduct.class);
            obCriteria.add(Restrictions
                .eq(BOMVersionProduct.PROPERTY_OBPBOMVBOMVERSION, bomVersion));
            obCriteria
                .add(Restrictions.eq(BOMVersionProduct.PROPERTY_PRODUCT, bom.getBOMProduct()));
            obCriteria.setMaxResults(1);
            bomVersionProduct = (BOMVersionProduct) obCriteria.uniqueResult();
          }
          if (bomVersionProduct != null) {
            bomVersionProduct = OBDal.getInstance().get(BOMVersionProduct.class,
                bomVersionProduct.getId());
            bomVersionProduct.setBOMQuantity(bom.getBOMQuantity());
            bomVersionProduct.setDescription(bom.getDescription());

          } else {
            bomVersionProduct = OBProvider.getInstance().get(BOMVersionProduct.class);
            bomVersionProduct.setOrganization(bom.getOrganization());
            bomVersionProduct.setProduct(bom.getBOMProduct());
            bomVersionProduct.setObpbomvBomVersion(bomVersion);
            bomVersionProduct.setBOMQuantity(bom.getBOMQuantity());
            bomVersionProduct.setLineNo(bom.getLineNo());
            bomVersionProduct.setDescription(bom.getDescription());
            bomVersionProduct.setCreatedBy(bom.getCreatedBy());
            bomVersionProduct.setUpdatedBy(bom.getUpdatedBy());
            OBDal.getInstance().save(bomVersionProduct);
          }
          updatedMaterials.add(bom.getBOMProduct().getId());
        }
      }
      bomVersion.setObpbomvBomverified(false);
      return getResponseBuilder().showMsgInProcessView(MessageType.SUCCESS,
          OBMessageUtils.messageBD("Success"), OBMessageUtils.messageBD("Success")).build();

    } catch (final Exception e) {
      log.error(e.getMessage());
      Throwable ex = DbUtility.getUnderlyingSQLException(e);
      String message = OBMessageUtils.messageBD(MessageCodeParser.parse(ex.getMessage()));
      return getResponseBuilder().showMsgInProcessView(MessageType.ERROR,
          OBMessageUtils.messageBD("Error"), message).build();
    } finally {
      OBContext.restorePreviousMode();
    }
  }
}
