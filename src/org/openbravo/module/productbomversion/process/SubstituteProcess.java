/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.module.productbomversion.process;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.client.application.process.ResponseActionsBuilder.MessageType;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductBOM;
import org.openbravo.model.common.plm.ProductSubstitute;
import org.openbravo.service.db.DbUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SubstituteProcess extends BaseProcessActionHandler {

  private static final Logger log = LoggerFactory.getLogger(SubstituteProcess.class);

  @Override
  protected JSONObject doExecute(Map<String, Object> parameters, String content) {

    OBContext.setAdminMode(true);
    try {
      JSONObject jsonData = new JSONObject(content);
      JSONObject jsonparams = jsonData.getJSONObject("_params");

      String productId = jsonData.getString("M_Product_ID");
      Product product = OBDal.getInstance().get(Product.class, productId);

      String substituteId = jsonparams.getString("Substitute_ID");
      Product productSubstitute = OBDal.getInstance().get(Product.class, substituteId);
      String substituteName = jsonparams.getString("Name");

      ProductSubstitute substituteProduct = OBProvider.getInstance().get(ProductSubstitute.class);
      substituteProduct.setProduct(product);
      substituteProduct.setSubstituteProduct(productSubstitute);
      substituteProduct.setOrganization(product.getOrganization());
      substituteProduct.setName(substituteName);
      OBDal.getInstance().save(substituteProduct);
      product.getProductSubstituteList().add(substituteProduct);
      OBDal.getInstance().save(product);

      if (findCycleInParents(product)) {
        product.getProductSubstituteList().remove(substituteProduct);
        OBDal.getInstance().save(product);
        OBDal.getInstance().remove(substituteProduct);
        throw new OBException("@LOOP_IN_BOM@");
      }
      return getResponseBuilder().showMsgInProcessView(MessageType.SUCCESS,
          OBMessageUtils.messageBD("Success"), OBMessageUtils.messageBD("Success")).build();

    } catch (final Exception e) {
      log.error(e.getMessage());
      Throwable ex = DbUtility.getUnderlyingSQLException(e);
      String message = OBMessageUtils.messageBD(MessageCodeParser.parse(ex.getMessage()));
      return getResponseBuilder().showMsgInProcessView(MessageType.ERROR,
          OBMessageUtils.messageBD("Error"), message).build();
    } finally {
      OBContext.restorePreviousMode();
    }

  }

  private boolean findCycleInParents(Product product) {
    Set<Product> visitedProducts = new TreeSet<>(CheckBomCyclesUtils.getProductComparator());
    visitedProducts.add(product);
    return findCycleInParentsAux(product, visitedProducts);

  }

  private boolean findCycleInParentsAux(Product product, Set<Product> visitedProducts) {

    if (!product.getProductBOMBOMProductList().isEmpty()) {
      for (ProductBOM productBOM : product.getProductBOMBOMProductList()) {
        Product parentProduct = productBOM.getProduct();
        Set<Product> visitedProductsAux = new TreeSet<>(CheckBomCyclesUtils.getProductComparator());
        visitedProductsAux.addAll(visitedProducts);
        if (visitedProductsAux.contains(parentProduct)) {
          return true;
        } else {
          visitedProductsAux.add(parentProduct);
        }
        if (findCycleInParentsAux(parentProduct, visitedProductsAux)) {
          return true;
        }
      }
    } else {
      List<Product> productParentList = new ArrayList<>();
      productParentList.add(product);

      List<ProductBOM> bomProductList = product.getProductBOMList();
      List<Product> productBomList = CheckBomCyclesUtils
          .convertListBomToListProduct(bomProductList);
      return CheckBomCyclesUtils.checkForcycles(productParentList, productBomList);
    }

    return false;
  }

}
