/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.module.productbomversion.process;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductBOM;
import org.openbravo.model.common.plm.ProductSubstitute;

public class CheckBomCyclesUtils {

  public static boolean checkForcycles(List<Product> productParentList, List<Product> productBomList) {
    List<Product> auxProductBomList = removeDuplicates(productBomList);
    return checkForcycles2(productParentList, auxProductBomList);
  }

  private static boolean checkForcycles2(List<Product> productParentList,
      List<Product> productBomList) {

    List<Product> auxProductBomList = new ArrayList<>();

    // Checks if some element of the first list appears in the second

    for (Product product : productParentList) {
      for (Product productBOM : productBomList) {
        if (product.getId().equals(productBOM.getId())) {
          return true;
        }
        for (ProductSubstitute productSubstitute : productBOM.getProductSubstituteList()) {
          if (product.getId().equals(productSubstitute.getSubstituteProduct().getId())) {
            return true;
          }
          if (productSubstitute.getSubstituteProduct().isBillOfMaterials()) {
            auxProductBomList.add(productSubstitute.getSubstituteProduct());
          }
        }
        if (productBOM.isBillOfMaterials()) {
          auxProductBomList.add(productBOM);
        }
      }
    }
    auxProductBomList = removeDuplicates(auxProductBomList);

    // If the elements are different, it checks recursively through the children of the second list
    // for cycles

    for (Product productBOM : auxProductBomList) {
      List<Product> auxProductParentList = new ArrayList<>();
      auxProductParentList.addAll(productParentList);
      auxProductParentList.add(productBOM);
      if (checkForcycles(auxProductParentList,
          convertListBomToListProduct(productBOM.getProductBOMList()))) {
        return true;
      }
    }
    return false;
  }

  public static List<Product> convertListBomToListProduct(List<ProductBOM> bomlist) {
    List<Product> productlist = new ArrayList<>();
    for (ProductBOM product : bomlist) {
      productlist.add(product.getBOMProduct());
    }
    return productlist;
  }

  private static List<Product> removeDuplicates(List<Product> products) {
    // Remove productBomList duplicates
    Set<Product> unique = new TreeSet<>(getProductComparator());
    unique.addAll(products);
    List<Product> result = new ArrayList<>();
    result.addAll(unique);
    return result;
  }

  /**
   * Create a Product comparator based on search key attribute
   * 
   * @return Product comparator object
   */
  public static Comparator<Product> getProductComparator() {
    return new Comparator<Product>() {

      @Override
      public int compare(Product p1, Product p2) {
        return p1.getSearchKey().compareTo(p2.getSearchKey());
      }

    };
  }
}
