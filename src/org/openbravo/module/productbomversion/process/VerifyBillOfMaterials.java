/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.module.productbomversion.process;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.client.application.process.ResponseActionsBuilder.MessageType;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductBOM;
import org.openbravo.service.db.DbUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VerifyBillOfMaterials extends BaseProcessActionHandler {

  private static final Logger log = LoggerFactory.getLogger(VerifyBillOfMaterials.class);

  @Override
  protected JSONObject doExecute(Map<String, Object> parameters, String content) {
    OBContext.setAdminMode(true);
    try {
      JSONObject jsonData = new JSONObject(content);
      String productId = jsonData.getString("M_Product_ID");
      Product product = OBDal.getInstance().get(Product.class, productId);

      List<ProductBOM> bomProductList = product.getProductBOMList();
      if (bomProductList.isEmpty()) {
        throw new OBException("@BOM_Without_Lines@");
      }

      for (ProductBOM productBom : bomProductList) {
        if (productBom.getBOMQuantity().intValue() < 0) {
          throw new OBException("@BOM_NegativeQty@");
        }
      }

      List<Product> productBomList = CheckBomCyclesUtils
          .convertListBomToListProduct(bomProductList);
      if (CheckBomCyclesUtils.checkForcycles(Arrays.asList(product), productBomList)) {
        throw new OBException("@LOOP_IN_BOM@");
      }

      product.setBOMVerified(true);
      return getResponseBuilder().showMsgInProcessView(MessageType.SUCCESS,
          OBMessageUtils.messageBD("Success"), OBMessageUtils.messageBD("Success")).build();

    } catch (final Exception e) {
      log.error(e.getMessage());
      Throwable ex = DbUtility.getUnderlyingSQLException(e);
      String message = OBMessageUtils.messageBD(MessageCodeParser.parse(ex.getMessage()));
      return getResponseBuilder().showMsgInProcessView(MessageType.ERROR,
          OBMessageUtils.messageBD("Error"), message).build();
    } finally {
      OBContext.restorePreviousMode();
    }
  }
}
