/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.module.productbomversion.process;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.client.application.process.ResponseActionsBuilder.MessageType;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.common.plm.Product;
import org.openbravo.module.productbomversion.BOMVersion;
import org.openbravo.module.productbomversion.BOMVersionProduct;
import org.openbravo.service.db.DbUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VerifyBomVersionProcess extends BaseProcessActionHandler {

  private static final Logger log = LoggerFactory.getLogger(VerifyBomVersionProcess.class);

  @Override
  protected JSONObject doExecute(Map<String, Object> parameters, String content) {

    OBContext.setAdminMode(true);
    try {
      JSONObject jsonData = new JSONObject(content);

      String productId = jsonData.getString("M_Product_ID");
      Product product = OBDal.getInstance().get(Product.class, productId);

      List<Product> productParentList = new ArrayList<>();
      productParentList.add(product);

      List<Product> productBomList = null;
      List<BOMVersionProduct> bomVersionProductList = null;
      BOMVersion bomVersion = null;

      String bomVersionId = jsonData.getString("Obpbomv_Bom_Version_ID");
      bomVersion = OBDal.getInstance().get(BOMVersion.class, bomVersionId);
      bomVersionProductList = bomVersion.getOBPBOMVBOMVersionProductList();
      productBomList = convertListProductBomVersionToListProduct(bomVersionProductList);

      if (productBomList.isEmpty()) {
        bomVersion.setObpbomvBomverified(false);
        throw new OBException("@OBPBOMV_BOMVersions_Without_Lines@");
      }

      for (BOMVersionProduct productBom : bomVersionProductList) {
        if (productBom.getBOMQuantity().floatValue() < 0) {
          bomVersion.setObpbomvBomverified(false);
          throw new OBException("@BOM_NegativeQty@");
        }
      }
      if (CheckBomCyclesUtils.checkForcycles(productParentList, productBomList)) {
        bomVersion.setObpbomvBomverified(false);
        throw new OBException("@LOOP_IN_BOM@");
      }
      bomVersion.setObpbomvBomverified(true);
      return getResponseBuilder().showMsgInProcessView(MessageType.SUCCESS,
          OBMessageUtils.messageBD("Success"), OBMessageUtils.messageBD("Success")).build();

    } catch (final Exception e) {
      log.error(e.getMessage());
      Throwable ex = DbUtility.getUnderlyingSQLException(e);
      String message = OBMessageUtils.messageBD(MessageCodeParser.parse(ex.getMessage()));
      return getResponseBuilder().showMsgInProcessView(MessageType.ERROR,
          OBMessageUtils.messageBD("Error"), message).build();
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private List<Product> convertListProductBomVersionToListProduct(
      List<BOMVersionProduct> productversionlist) {
    List<Product> productlist = new ArrayList<>();
    for (BOMVersionProduct product : productversionlist) {
      productlist.add(product.getProduct());
    }
    return productlist;
  }
}
